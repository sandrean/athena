/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/** @file ElementLinkCnv_p3.icc
 *  @brief This file contains the implementation for the ElementLinkCnv_p3 template methods.
 *  @author Marcin.Nowak@cern.ch
 **/

#include <stdexcept>

#include "AthLinks/ElementLink.h"
#include "AthenaKernel/IThinningSvc.h"
#include "DataModelAthenaPool/getThinningCache.h"
//#define ELLINK_DEBUG


template <typename LINK_TYPE>
void ElementLinkCnv_p3<LINK_TYPE>::
transToPers(const Link_t& trans, PersLink_t& pers,
            const SG::ThinningCache* /*cache*/,
            [[maybe_unused]] MsgStream& msg ) const
{
   if( !trans.isDefault() ) {
      IThinningSvc * thinningSvc = IThinningSvc::instance();
      const SG::DataProxy* dp = trans.proxy();
      const bool thinning = thinningSvc && 
        (dp 
         ? thinningSvc->thinningOccurred(dp) 
         // otherwise, use the event-level one
         : thinningSvc->thinningOccurred());
      if (thinning) {
        // the not so simple case: some thinning occurred
        
        // here is the problem: in case the ElementLink was directly created w/
        // only a pointer to the element, _and_ if the the pointed at element 
        // has been thinned away, EL::index() will throw b/c
        // IndexingPolicy::setIndex will fail.
        std::size_t idx = IThinningSvc::RemovedIdx;
        try {
          idx = trans.index();
        } catch ( const SG::maybe_thinning_error& err ) {
          // ok. that's the corner case we talked about above.
#ifdef  ELLINK_DEBUG
          msg << MSG::DEBUG << "caught a maybe_thinning_error: ["
              << err.what() << "]"
              << endmsg
              << "(this is an expected case of the EL-state-phase-space "
              << "when thinning is active)"
              << endmsg;
#endif
        }
        const std::size_t persIdx = dp ?
          thinningSvc->index(dp, idx) :
          thinningSvc->index(trans.getStorableObjectPointer(), idx);
        
        if( IThinningSvc::RemovedIdx == persIdx )
        {
          // this element has been thinned away. So the persistent equivalent
          // of a null pointer is a default persistent pointer.
          pers = PersLink_t();
        }
        else {
          Link_t tmp = trans;
          tmp.toPersistent();
          pers.m_SGKeyHash       = tmp.key(); 
          pers.m_elementIndex    = persIdx;
        }
#ifdef  ELLINK_DEBUG
        msg << MSG::INFO << "ElementLinkCnv_p3::transToPer(): SG Container="
            << ", Key Hash=" << pers.m_SGKeyHash
            << ", IDX=" << pers.m_elementIndex << endmsg;
#endif
        return;
      }

      // No thinning.
      Link_t tmp = trans;
      tmp.toPersistent();
      pers.m_SGKeyHash        = tmp.key(); 
      pers.m_elementIndex     = tmp.index();
#ifdef  ELLINK_DEBUG
      msg << MSG::INFO << "ElementLinkCnv_p3::transToPer(): SG Container="
          << ", Key Hash=" << pers.m_SGKeyHash
          << ", IDX=" << pers.m_elementIndex << endmsg;
#endif
   }
}


template <typename LINK_TYPE>
void ElementLinkCnv_p3<LINK_TYPE>::
transToPers(const Link_t& trans, PersLink_t& pers,
            MsgStream& msg ) const
{
  transToPers (trans, pers,
               DataModelAthenaPool::getThinningCache(),
               msg);
}


template <typename LINK_TYPE >
void ElementLinkCnv_p3< LINK_TYPE >
::persToTrans(const PersLink_t& pers, Link_t& trans,
              [[maybe_unused]] MsgStream& msg) const
{
   if( pers.m_SGKeyHash != 0 ) {
#ifdef  ELLINK_DEBUG
      msg << MSG::DEBUG << "ElementLinkCnv_p3::PersToTrans(): SGContainer hash="
	  << pers.m_SGKeyHash << ", IDX=" << pers.m_elementIndex << endmsg;
#endif
      trans = Link_t( (typename Link_t::sgkey_t)pers.m_SGKeyHash, pers.m_elementIndex);
   }
   else {
#ifdef  ELLINK_DEBUG
      msg << MSG::DEBUG << "ElementLinkCnv_p3::PersToTrans(): reading EL in Default state" << endmsg;
#endif
      // set the transient ELink to the default state.
      trans.reset();
   } 
}


template <typename LINK_TYPE>
void ElementLinkCnv_p3<LINK_TYPE>::
transToPers(const Link_t* trans, PersLink_t* pers, MsgStream& msg ) const
{
  return transToPers (*trans, *pers, msg);
}


template <typename LINK_TYPE >
void ElementLinkCnv_p3< LINK_TYPE >
::persToTrans(const PersLink_t* pers, Link_t* trans, MsgStream& msg) const
{
  return persToTrans (*pers, *trans, msg);
}
